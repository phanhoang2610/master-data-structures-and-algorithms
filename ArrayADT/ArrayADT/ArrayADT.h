//
//  ArrayADT.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/19.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef ArrayADT_h
#define ArrayADT_h

#include <stdio.h>
#include <stdlib.h>

#define MAX 100

struct Array
{
    int A[ MAX ];
    int size;
    int length;
};

/* *********************************************** */
/* *           Array's Operations Prototype        */
/* *********************************************** */
void Display( struct Array );
void DisplayAll( struct Array ); // DEBUG
void Insert( struct Array *, int, int);
void Append( struct Array *, int );
void Delete( struct Array *, int );
int LinearSearch( struct Array *, int );
int BinarySearch( struct Array *arr, int, int, int );

int Get( struct Array *, int ); // Get value at index
void Set( struct Array *, int, int ); // Set value at index
int Sum( struct Array * ); // Get total value of Array
float Avg( struct Array * ); // Get Average value of Array
int Max( struct Array * ); // Get Max value of Array
int Min( struct Array * ); // Get Min value of Array

void Reverse( struct Array * );
struct Array* Merge( struct Array *, struct Array * );
struct Array* Union( struct Array *, struct Array * );
struct Array* Intersection( struct Array *, struct Array * );
struct Array* Difference( struct Array *, struct Array * );

void Find_duplicates_in_sorted_array( struct Array * ); // Finding duplicated items of Array

int isSorted( struct Array * );

void swap( int *, int * );

/* *********************************************** */
/* *         TEST FUNCTIONS PROTOTYPE            * */
/* *********************************************** */
void test_display_function( void ); // TEST DISPLAY
void test_insert_function( void ); // TEST INSERT
void test_append_function( void ); // TEST APPEND
void test_delete_function( void ); // TEST DELETE
void test_linear_search_function( void ); // TEST LINEAR SEARCH
void test_binary_search_function( void ); // TEST BINARY SEARCH
void test_get_function( void ); // TEST GET ELEMENT VALUE AT A POSITION
void test_set_function( void ); // TEST SET ELEMENT VALUE AT A POSITION
void test_get_max_value( void ); // TEST GET MAX VALUE ELEMENT
void test_get_min_value( void ); // TEST GET MIN VALUE ELEMENT
void test_get_average_value( void ); // TEST GET AVERAGE VALUE OF ARRAY
void test_get_reversed_array( void ); // TEST GET REVERSED ARRAY OF ARRAY
void test_is_sorted_function( void ); // TEST CHECK IF ARRAY IS SORTED OR NOT
void test_merge_arrays_function( void ); // TEST MERGE 2 SORTED ARRAYS
void test_sets_fuctions( void ); // TEST SETS FUNCTIONS
void test_find_duplicates_fuctions( void ); // TEST FINDING DUPLICATED ITEMS OF ARRAY
#endif /* ArrayADT_h */
