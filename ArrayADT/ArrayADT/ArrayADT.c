//
//  ArrayADT.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/19.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "ArrayADT.h"

/* ********************************************************************************
 *  Array ADT : Array Abstract Data Structure
 *
 *   Data Struct:
 *     1. Array Space
 *     2. Size
 *     3. Length
 *
 *   Operations:
 *     1. Display
 *     2. Add/Append
 *     3. Insert
 *     4. Delete
 *     5. Search (Linear/Binary)
 *     6. Get
 *     7. Set
 *     8. Max/Min
 *     9. Reverse
 *     10. Get duplicates
 * ********************************************************************************/


/* ********************************************************************************
 * NAME         :  void DisplayAll( struct Array arr )
 *
 * PARAMETERS   :  struct Array arr    :    Displayed Array Data
 *
 * DESCRIPTION  :  Print data for DEBUGGING.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void DisplayAll( struct Array arr )
{
    int cnt;
    printf("-----------------------\n");
    printf("Array size: %d\n", arr.size);
    printf("\nArray length: %d\n", arr.length);
    printf("\nElements are: ");
    for ( cnt = 0 ; cnt < arr.length ; cnt++ )
    {
        printf("%d ", arr.A[ cnt ]);
    }
    printf("\n-----------------------\n");
}



/* ********************************************************************************
 * NAME         :  void Display( struct Array arr )
 *
 * PARAMETERS   :  struct Array arr    :    Array data
 *
 * DESCRIPTION  :  Print all item of array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Display( struct Array arr )
{
    int cnt;
    for ( cnt = 0 ; cnt < arr.length ; cnt++ )
    {
        printf("%d ", arr.A[ cnt ]);
    }
    printf("\n\n");
}


/* ********************************************************************************
 * NAME         :  void Append( struct Array *arr, int value )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int value            :    Appended Value
 *
 * DESCRIPTION  :  Append a number to array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Append( struct Array *arr, int value )
{
    if ( arr->length < arr->size )
    {
        arr->A[ arr->length++ ] = value;
    }
}


/* ********************************************************************************
 * NAME         :  void Delete( struct Array *arr, int index )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int index            :    Deleted index number
 *
 * DESCRIPTION  :  Delete a number from array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Delete( struct Array *arr, int index )
{
    int cnt;
    
    if ( index >= 0 && index < arr->length )
    {
        for ( cnt = index; cnt < arr->length - 1; cnt++ )
        {
            arr->A[ cnt ] = arr->A[ cnt + 1 ];
        }
        arr->length--;
    }
    else
    {
        // Error
        printf("[ERR] Invalid index value !\n");
    }
}


/* ********************************************************************************
 * NAME         :  void Insert( struct Array *arr, int index, int value )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int index            :    Inserted index
 *                 int value            :    Inserted value
 *
 * DESCRIPTION  :  Insert a number to a specified index of array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Insert( struct Array *arr, int index, int value )
{
    int cnt;
    
    if ( index >= 0 && index <= arr->length )
    {
        for ( cnt = arr->length; cnt > index; cnt-- )
        {
            arr->A[ cnt ] = arr->A[ cnt - 1 ];
        }
        arr->A[ index ] = value;
        arr->length++;
    }
    else
    {
        // Error
        printf("[ERR] Invalid index value !\n");
    }
}


/* ********************************************************************************
 * NAME         :  int LinearSearch( struct Array *arr, int value )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int value            :    Searched value
 *
 * DESCRIPTION  :  Search a number in array and return the index number.
 *
 * RETURN       :  Index number of searched value.
 *
 * ********************************************************************************/
int LinearSearch( struct Array *arr, int value )
{
    int cnt;
    for ( cnt = 0; cnt < arr->length; cnt++ )
    {
        if ( value == arr->A[ cnt ] )
        {
            // Swap found position with head (front) of Array to improve the Linear Search speed for next time
            swap( &arr->A[ cnt ], &arr->A[ 0 ] );
            return cnt;
        }
    }
    return -1;
}


/* ********************************************************************************
 * NAME         :  void swap( int *a, int *b )
 *
 * PARAMETERS   :  int *a    :    Number 1
 *                 int *b    :    Number 2
 *
 * DESCRIPTION  :  Swap 2 number value.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void swap( int * a, int * b )
{
    int tmp;
    
    tmp = *a;
    *a = *b;
    *b = tmp;
}


/* ********************************************************************************
 * NAME         :  int BinarySearch( struct Array *arr, int val, int l, int h )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int val              :    Searched value
 *                 int l                :    'Low' index value
 *                 int h                :    'High' index value
 *
 * DESCRIPTION  :  Search a sorted array by repeatedly dividing the search interval in half.
 *
 * RETURN       :  None.
 *
 * NOTE         :  Best case O(1)
 *                 Worst case O(log(n))
 *
 * ********************************************************************************/
int BinarySearch( struct Array *arr, int val, int l, int h )
{
    int mid = ( l + h ) / 2;
    // printf(" l = %d , h = %d, mid = %d\n", l, h, mid); // DEBUG

    if ( l <= h )
    {
        if ( arr->A[ mid ] == val )
        {
            return mid;
        }
        else if ( arr->A[ mid ] < val )
        {
            return BinarySearch( arr, val, mid + 1, h );
        }
        else
        {
            return BinarySearch( arr, val, l, mid - 1 );
        }
    }

    return -1;
}


/* ********************************************************************************
 * NAME         :  int Get( struct Array *arr, int index )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *                 int index            :    Index number
 *
 * DESCRIPTION  :  Get value at specified index of array.
 *
 * RETURN       :  Value at specified index of array.
 *
 * ********************************************************************************/
int Get( struct Array *arr, int index )
{
    if ( index >=0 && index < arr->length )
    {
        return arr->A[ index ];
    }
    return -1;
}


/* ********************************************************************************
 * NAME         :  void Set( struct Array *arr, int index, int value )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Data
 *                 int index            :    Index number
 *                 int value            :    Set value
 *
 * DESCRIPTION  :  Set value at specified index of array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Set( struct Array *arr, int index, int value )
{
    if ( index >=0 && index < arr->length )
    {
        arr->A[ index ] = value;
    }
}


/* ********************************************************************************
 * NAME         :  int Max( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Get maximum value of an array.
 *
 * RETURN       :  Maximum value of an array.
 *
 * ********************************************************************************/
int Max( struct Array *arr )
{
    int max_val = arr->A[ 0 ];
    int cnt;
    
    for ( cnt = 1; cnt < arr->length; cnt++ )
    {
        if ( max_val < arr->A[ cnt ] )
        {
            max_val = arr->A[ cnt ];
        }
    }

    return max_val;
}


/* ********************************************************************************
 * NAME         :  int Min( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Get maximum value of an array.
 *
 * RETURN       :  Maximum value of an array.
 *
 * ********************************************************************************/
int Min( struct Array *arr )
{
    int min_val = arr->A[ 0 ];
    int cnt;
    
    for ( cnt = 1; cnt < arr->length; cnt++ )
    {
        if ( min_val > arr->A[ cnt ] )
        {
            min_val = arr->A[ cnt ];
        }
    }

    return min_val;
}


/* ********************************************************************************
 * NAME         :  int Sum( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Get total value of an array.
 *
 * RETURN       :  Total value of an array.
 *
 * ********************************************************************************/
int Sum( struct Array *arr )
{
    int sum = 0;
    int cnt;
    
    for ( cnt = 0; cnt < arr->length; cnt++ )
    {
        sum += arr->A[ cnt ];
    }
    return sum;
}


/* ********************************************************************************
 * NAME         :  int Avg( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Get average value of an array.
 *
 * RETURN       :  Average value of an array.
 *
 * ********************************************************************************/
float Avg( struct Array *arr )
{
    return ( ( float ) Sum( arr ) / arr->length );
}


/* ********************************************************************************
 * NAME         :  void Reverse( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Reverse array.
 *
 * RETURN       :  None.
 *
 * ********************************************************************************/
void Reverse( struct Array *arr )
{
    int cnt1, cnt2;
    for ( cnt1 = 0, cnt2 = arr->length - 1; cnt1 < cnt2; cnt1++, cnt2-- )
    {
        // Swap item
        swap( &arr->A[ cnt1 ], &arr->A[ cnt2 ] );
    }
}


/* ********************************************************************************
 * NAME         :  int isSorted( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Check if array is sorted or not.
 *
 * RETURN       :  0 if array is not sorted.
 *                 1 if array is not sorted.
 *
 * ********************************************************************************/
int isSorted( struct Array *arr )
{
    int cnt;
    for ( cnt = 0 ; cnt < ( arr->length - 1 ) ; cnt++ )
    {
        if ( arr->A[ cnt ] > arr->A[ cnt + 1] )
        {
            return 0;
        }
    }
    return 1;
}


/* ********************************************************************************
 * NAME         :  struct Array* Merge( struct Array *arr1, struct Array *arr2 )
 *
 * PARAMETERS   :  struct Array *arr1    :    Array 1 Pointer
 *                 struct Array *arr2    :    Array 2 Pointer
 *
 * DESCRIPTION  :  Merge 2 arrays to 1 array and return the new array pointer.
 *
 * RETURN       :  New merged array pointer.
 *
 * ********************************************************************************/
struct Array* Merge( struct Array *arr1, struct Array *arr2 )
{
    if ( !isSorted(arr1) || !isSorted(arr2))
    {
        printf("[ERR] Only merge for sorted array!\n");
        return NULL;
    }
    else
    {
        struct Array *new_arr = ( struct Array * ) malloc( sizeof ( struct Array ) );

        int cnt1 = 0;    // Array 1 item count number
        int cnt2 = 0;    // Array 2 item count number
        int ncnt = 0;    // New Array item count number

        while ( cnt1 < arr1->length && cnt2 < arr2->length )
        {
            if ( arr1->A[ cnt1 ] < arr2->A[ cnt2 ] )
            {
                new_arr->A[ ncnt++ ] = arr1->A[ cnt1++ ];
            }
            else
            {
                new_arr->A[ ncnt++ ] = arr2->A[ cnt2++ ];
            }
        }
        
        // Copy remaining item of array 1 and array 2 to new array
        for ( ; cnt1 < arr1->length ; cnt1++ )
        {
            new_arr->A[ ncnt++ ] = arr1->A[ cnt1 ];
        }
        
        for ( ; cnt2 < arr2->length ; cnt2++ )
        {
            new_arr->A[ ncnt++ ] = arr2->A[ cnt2 ];
        }
        
        new_arr->size = 10;
        new_arr->length = arr1->length + arr2->length;
        
        return new_arr;
    }
}


/* ********************************************************************************
 * NAME         :  struct Array* Merge( struct Array *arr1, struct Array *arr2 )
 *
 * PARAMETERS   :  struct Array *arr1    :    Array 1 Pointer
 *                 struct Array *arr2    :    Array 2 Pointer
 *
 * DESCRIPTION  :  Merge 2 arrays to 1 array and return the new array pointer.
 *
 * RETURN       :  New merged struct array pointer.
 *
 * NOTE         :   Example:
 *                      A = { 1,2,3 }
 *                      B = { 2,3,4,5 }
 *
 *                     => A ∪ B = { 1,2,3,4,5 }          ∪: Union Symbol
 *
 * ********************************************************************************/
struct Array* Union( struct Array *arr1, struct Array *arr2 )
{
    struct Array *new_arr = ( struct Array * ) malloc( sizeof ( struct Array ) );
    int cnt1 = 0;    // Array 1 item count number
    int cnt2 = 0;    // Array 2 item count number
    int ncnt = 0;    // New Array item count number
    while ( cnt1 < arr1->length && cnt2 < arr2->length )
    {
        if ( arr1->A[ cnt1 ] < arr2->A[ cnt2 ] )
        {
            new_arr->A[ ncnt++ ] = arr1->A[ cnt1++ ];
        }
        else if ( arr1->A[ cnt1 ] > arr2->A[ cnt2 ] )
        {
            new_arr->A[ ncnt++ ] = arr2->A[ cnt2++ ];
        }
        else
        {
            new_arr->A[ ncnt++ ] = arr1->A[ cnt1++ ];
            cnt2++;
        }
    }
    
    // Copy remaining item of array 1 and array 2 to new array
    for ( ; cnt1 < arr1->length ; cnt1++ )
    {
        new_arr->A[ ncnt++ ] = arr1->A[ cnt1 ];
    }
    
    for ( ; cnt2 < arr2->length ; cnt2++ )
    {
        new_arr->A[ ncnt++ ] = arr2->A[ cnt2 ];
    }

    new_arr->length = ncnt;
    new_arr->size = 10;
    
    return new_arr;
}

/* ********************************************************************************
 * NAME         :  struct Array* Merge( struct Array *arr1, struct Array *arr2 )
 *
 * PARAMETERS   :  struct Array *arr1    :    Array 1 Pointer
 *                 struct Array *arr2    :    Array 2 Pointer
 *
 * DESCRIPTION  :  Get intersection from 2 arrays and return the new struct array pointer.
 *
 * RETURN       :  New struct array pointer.
 *
 * NOTE         :  Example:
 *                     A = { 1,2,3 }
 *                     B = { 2,3,4,5 }
 *
 *                     => A ∩ B = { 2,3 }          ∩: Intersection Symbol
 *
 * ********************************************************************************/
struct Array* Intersection( struct Array *arr1, struct Array *arr2 )
{
    struct Array *new_arr = ( struct Array * ) malloc ( sizeof ( struct Array ) );
    int cnt1 = 0;    // Array 1 item count number
    int cnt2 = 0;    // Array 2 item count number
    int ncnt = 0;    // New Array item count number
    while ( cnt1 < arr1->length && cnt2 < arr2->length )
    {
        if ( arr1->A[ cnt1 ] < arr2->A[ cnt2 ] )
        {
            cnt1++;
        }
        else if ( arr1->A[ cnt1 ] > arr2->A[ cnt2 ] )
        {
            cnt2++;
        }
        else
        {
            new_arr->A[ ncnt++ ] = arr1->A[ cnt1++ ];
            cnt2++;
        }
    }
    
    new_arr->length = ncnt;
    new_arr->size = 10;
    
    return new_arr;
}


/* ********************************************************************************
 * NAME         :  struct Array* Merge( struct Array *arr1, struct Array *arr2 )
 *
 * PARAMETERS   :  struct Array *arr1    :    Array 1 Pointer
 *                 struct Array *arr2    :    Array 2 Pointer
 *
 * DESCRIPTION  :  Get difference from 2 arrays and return the new struct array pointer.
 *
 * RETURN       :  New struct array pointer.
 *
 * NOTE         :  Example:
 *                     A = { 1,2,3 }
 *                     B = { 2,3,4,5 }
 *
 *                     => A - B = { 1,4,5 }          - : Difference Symbol
 *
 * ********************************************************************************/
struct Array* Difference( struct Array *arr1, struct Array *arr2 )
{
    struct Array *new_arr = ( struct Array * ) malloc ( sizeof ( struct Array ) );
    int cnt1 = 0;    // Array 1 item count number
    int cnt2 = 0;    // Array 2 item count number
    int ncnt = 0;    // New Array item count number
    while ( cnt1 < arr1->length && cnt2 < arr2->length )
    {
        if ( arr1->A[ cnt1 ] < arr2->A[ cnt2 ] )
        {
            new_arr->A[ ncnt++ ] = arr1->A[ cnt1++ ];
        }
        else if ( arr1->A[ cnt1 ] > arr2->A[ cnt2 ] )
        {
            new_arr->A[ ncnt++ ] = arr2->A[ cnt2++ ];
        }
        else
        {
            cnt1++;
            cnt2++;
        }
    }
    
    // Copy remaining item of array 1 and array 2 to new array
    for ( ; cnt1 < arr1->length ; cnt1++ )
    {
        new_arr->A[ ncnt++ ] = arr1->A[ cnt1 ];
    }
    
    for ( ; cnt2 < arr2->length ; cnt2++ )
    {
        new_arr->A[ ncnt++ ] = arr2->A[ cnt2 ];
    }
    
    new_arr->length = ncnt;
    new_arr->size = 10;
    
    return new_arr;
}


/* ********************************************************************************
 * NAME         :  void Find_duplicates_in_sorted_array( struct Array *arr )
 *
 * PARAMETERS   :  struct Array *arr    :    Array Pointer
 *
 * DESCRIPTION  :  Find duplicated value of array.
 *
 * RETURN       :  NONE.
 *
 * ********************************************************************************/
void Find_duplicates_in_sorted_array( struct Array *arr )
{
    int i_cnt = 0; // Count number
    int j_cnt = 1; // Count number

    for ( i_cnt = 0 ; i_cnt < arr->length ; j_cnt++ )
    {
        if ( arr->A[ i_cnt ] != arr->A[ j_cnt ] )
        {
            if ( j_cnt - i_cnt > 1 )
            {
                printf("%d : %d ( times )\n", arr->A[ i_cnt ], j_cnt - i_cnt );
            }
            i_cnt = j_cnt;
        }
    }
    
    printf("\n");
}


/* ********************************************************* */
/* *                                                       * */
/* *       ALL TEST FUNCTIONS IMPLEMENTATION               * */
/* *                                                       * */
/* ********************************************************* */

void test_display_function()
{
    
    struct Array arr = { { 10, 8, 12, 9, 5, 15 }, 10, 6 }; // Inits an array with 6 items
    printf("***** Creat An Array *****\n");
    Display( arr );
}


void test_insert_function()
{
    
    struct Array arr = { { 10, 8, 12, 9, 5, 15 }, 10, 6 }; // Inits an array with 6 items
    int insert_pos = 1;
    int insert_val = 15;
    printf("***** Insert value %d to position %d of Array *****\n", insert_val, insert_pos);
    Insert( &arr, insert_pos, insert_val );
    Display( arr );
}

void test_append_function()
{
    struct Array arr = { { 10, 8, 12, 9, 5, 15 }, 10, 6 }; // Inits an array with 6 items
    int append_val = 5;
    printf("***** Append value %d to Array *****\n", append_val);
    Append( &arr, append_val );
    Display( arr );
}

void test_delete_function()
{
    struct Array arr = { { 10, 8, 12, 9, 5, 15 }, 10, 6 }; // Inits an array with 6 items
    int del_pos = 2;
    printf("***** Delete item at position %d of Array *****\n", del_pos);
    Delete( &arr, del_pos );
    Display( arr );
}

void test_linear_search_function()
{
    struct Array arr = { { 10, 8, 12, 9, 5, 15 }, 10, 6 }; // Inits an array with 6 items
    int search_val = 5;
    int search_res = -1; // Search result ( -1: Not found )
    printf("***** Search value %d in Array *****\n", search_val);
    
    search_res = LinearSearch( &arr, search_val );
    
    if ( search_res!= -1 )
    {
        printf("Position: %d\n\n", search_res);
    }
    else
    {
        printf("Not found!\n\n");
    }
}

void test_binary_search_function()
{
    struct Array arr = { { 1,3,6,7,10,15,19,21,33,38 }, 20, 10 }; // Create new sorted array with 10 items
    printf("***** Create New Array *****\n");
    Display( arr );

    int bin_search_val = 19; // Searching Value
    int low = 0;
    int high = arr.length - 1;
    
    int bin_search_res = -1;
    
    printf("***** Binary search value %d in Array *****\n", bin_search_val);
    
    bin_search_res = BinarySearch( &arr, bin_search_val, low, high );
    
    if ( bin_search_res != -1 )
    {
        printf("Position: %d\n\n", bin_search_res);
    }
    else
    {
        printf("Not found!\n\n");
    }
}

void test_get_function()
{
    struct Array arr = { { 1,3,6,7,10,15,19,21,33,38 }, 20, 10 }; // Create new sorted array with 10 items
    int get_index = 6;
    int get_res = -1;
    
    printf("***** Get value at index %d from Array *****\n", get_index);
    get_res = Get( &arr, get_index );
    printf("%d\n\n", get_res);
}

void test_set_function()
{
    struct Array arr = { { 1,3,6,7,10,15,19,21,33,38 }, 20, 10 }; // Create new sorted array with 10 items
    int set_index = 4;
    int set_value = 12;
    
    printf("***** Set value at index %d to %d *****\n", set_index, set_value );
    Set( &arr, set_index, set_value );
    Display( arr );
}

void test_get_max_value()
{
    struct Array arr = { { 10,5,4,1,6,21,33,76,20,7 }, 20, 10 }; // Create new array with 10 items
    printf("***** Create New Array *****\n");
    Display( arr );
    
    int max_res = -1;
    
    printf("***** Get MAX value of array *****\n");
    max_res = Max( &arr );
    printf("%d\n\n", max_res);
    
}

void test_get_min_value()
{
    struct Array arr = { { 10,5,4,1,6,21,33,76,20,7 }, 20, 10 }; // Create new array with 10 items
    int min_res = -1;
    
    printf("***** Get MIN value of array *****\n");
    min_res = Min( &arr );
    printf("%d\n\n", min_res);
}

void test_get_average_value()
{
    struct Array arr = { { 10,5,4,1,6,21,33,76,20,7 }, 20, 10 }; // Create new array with 10 items
    float avg_res = -1.0F;
    
    printf("***** Get AVERAGE value of array *****\n");
    avg_res = Avg( &arr );
    printf("%f\n\n", avg_res);

}

void test_get_reversed_array()
{
    struct Array arr = { { 10,5,4,1,6,21,33,76,20,7 }, 20, 10 }; // Create new array with 10 items
    printf("***** Get Reversed array *****\n");
    Reverse( &arr );
    Display( arr );
}

void test_is_sorted_function()
{
    struct Array arr1 = { { 10,5,4,1,6,21,33,76,20,7 }, 20, 10 }; // Create new array with 10 items
    printf("***** Check if below array is sorted or not? *****\n");
    printf("Array 1: ");
    Display( arr1 );
    if ( isSorted( &arr1 ) )
    {
        printf("=> This array is sorted.\n\n");
    }
    else
    {
        printf("=> This array is not sorted.\n\n");
    }
    
    printf("\n");
    
    struct Array arr2 = { { 1,4,5,6,7,10,20,21,33,76 }, 20, 10 }; // Create new array with 10 items
    printf("Array 2: ");
    Display( arr2 );
    if ( isSorted( &arr2 ) )
    {
        printf("=> This array is sorted.\n\n");
    }
    else
    {
        printf("=> This array is not sorted.\n\n");
    }
}

void test_merge_arrays_function()
{
    printf("***** Merge 2 arrays below: *****\n");
    printf("Array 1 : ");
    struct Array arr1 = { { 3, 3, 6, 7, 10, 12, 13 }, 10, 7};
    Display( arr1 );
    
    printf("Array 2 : ");
    struct Array arr2 = { { 2, 4, 5, 9, 12 }, 10, 5};
    Display( arr2 );
    
    struct Array *arr3;

    arr3 = Merge( &arr1, &arr2 ); // Merges arr1 and arr2 to arr3
    printf("New Array : ");
    Display( *arr3 );
    
    free( arr3 );
    arr3 = NULL;
}

void test_sets_fuctions()
{
    printf("***** Get Union/Intersection/Difference Array of 2 arrays below: *****\n");
    printf("Array 1 : ");
    struct Array arr1 = { { 4, 5, 6, 7, 8 }, 10, 5};
    Display( arr1 );
    
    printf("Array 2 : ");
    struct Array arr2 = { { 1, 2, 3, 4, 5 }, 10, 5 };
    Display( arr2 );
    
    struct Array *arr3;

    printf("=> Union : ");
    arr3 = Union( &arr1, &arr2 ); // Get Union array of arr1 and arr2
    Display( *arr3 ); // True result is { 1,2,3,4,5,6,7,8}
    
    printf("=> Intersection : ");
    arr3 = Intersection( &arr1, &arr2 ); // Get Intersection array of arr1 and arr2
    Display( *arr3 ); // True result is { 4,5 }
    
    printf("=> Difference : ");
    arr3 = Difference( &arr1, &arr2 ); // Get Difference array of arr1 and arr2
    Display( *arr3 ); // True result is { 1,2,3,6,7,8 }
    
    free( arr3 );
    arr3 = NULL;
}


void test_find_duplicates_fuctions()
{
    printf("***** Find duplicated items of array below: *****\n");
    struct Array arr = { { 1,1,2,2,9,14,14,14,14,25,29,30,30,30,30 }, 15, 15 }; // Inits an array with 15 items
    Display( arr );
    
    printf("Duplicated item(s) :\n");
    Find_duplicates_in_sorted_array( &arr );
}


int main( int argc, const char * argv[] )
{
    test_display_function(); // TEST DISPLAY
    test_insert_function(); // TEST INSERT
    test_append_function(); // TEST APPEND
    test_delete_function(); // TEST DELETE
    test_linear_search_function(); // TEST LINEAR SEARCH
    test_binary_search_function(); // TEST BINARY SEARCH
    test_get_function(); // TEST GET ELEMENT VALUE AT A POSITION
    test_set_function(); // TEST SET ELEMENT VALUE AT A POSITION
    test_get_max_value(); // TEST GET MAX VALUE ELEMENT
    test_get_min_value(); // TEST GET MIN VALUE ELEMENT
    test_get_average_value(); // TEST GET AVERAGE VALUE OF ARRAY
    test_get_reversed_array(); // TEST GET REVERSED ARRAY OF ARRAY
    test_is_sorted_function(); // TEST CHECK IF ARRAY IS SORTED OR NOT
    test_merge_arrays_function(); // TEST MERGE 2 SORTED ARRAYS
    test_sets_fuctions(); // TEST GET UNION/INTERSECTION/DIFFERENCE OF 2 ARRAYS
    test_find_duplicates_fuctions(); // TEST FINDING DUPLICATED ITEMS OF ARRAY

    // End of main function
    return 0;
}
