//
//  Power_Using_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Power_Using_Recursion_h
#define Power_Using_Recursion_h

#include <stdio.h>

int power_using_recursion_func1( int, int );
int power_using_recursion_func2( int, int );
void test_power_using_recursion_func( int, int );


#endif /* Power_Using_Recursion_h */
