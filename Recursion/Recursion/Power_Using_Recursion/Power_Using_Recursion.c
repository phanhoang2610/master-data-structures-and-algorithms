//
//  Power_Using_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Power_Using_Recursion.h"

/* ********************************************************************************
 *  NAME         :  int power_using_recursion_func1( int base, int exponent )
 *
 *  DESCRIPTION  :  Calculates (base) ^ (exponent) value.
 *
 *  PARAMETERS   :  int    base        ( Base number )
 *                  int    exponent    ( Exponent number )
 *
 *  RETURN       :  (base) ^ (exponent) value
 *
 * ********************************************************************************/
int power_using_recursion_func1( int base, int exponent )
{
    if ( exponent == 0 )
    {
        return 1;    // n^0 = 1
    }
    else
    {
        exponent--;
        return power_using_recursion_func1( base, exponent ) * base;
    }
}


/* ********************************************************************************
*  NAME         :  int power_using_recursion_func1( int base, int exponent )
*
*  DESCRIPTION  :  Calculates (base) ^ (exponent) value. ( Faster version )
*
*  PARAMETERS   :  int    base        ( Base number )
*                  int    exponent    ( Exponent number )
*
*  RETURN       :  (base) ^ (exponent) value
*
* ********************************************************************************/
int power_using_recursion_func2( int base, int exponent )
{
    if ( exponent == 0 )
    {
        return 1;    // n^0 = 1
    }
    
    if ( ( exponent % 2 ) == 0 ) // If exponent is even number
    {
        return power_using_recursion_func2( base * base, exponent / 2);
    }
    else // If exponent is odd number
    {
        return base * power_using_recursion_func2( base * base, ( exponent - 1 ) / 2 );
    }
}


/* ********************************************************************************
 *  NAME         :  void test_power_using_recursion_func( int base, int exponent )
 *
 *  DESCRIPTION  :  Tests result of power calculation
 *
 *  PARAMETERS   :  int    base        ( Base number )
 *                  int    exponent    ( Exponent number )
 *
 *  RETURN       :  None
 *
 * ********************************************************************************/
void test_power_using_recursion_func( int base, int exponent )
{
    int value = 0;
    
    value = power_using_recursion_func1( base, exponent );
    printf("Power Using Recursion Result: %d^%d = %d\n", base, exponent, value);
    
    value = power_using_recursion_func2( base, exponent );
    printf("Power Using Recursion Result(Fast Method): %d^%d = %d\n", base, exponent, value);
    
    printf("\n");
}
