//
//  Tail_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Tail_Recursion_h
#define Tail_Recursion_h

#include <stdio.h>

void tail_recursion_func( int );
void test_tail_recursion_func( int );

#endif /* Tail_Recursion_h */
