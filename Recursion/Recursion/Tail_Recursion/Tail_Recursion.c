//
//  Tail_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Tail_Recursion.h"

/* ********************************************************************************
 * NAME         :  void tail_recursion_func( int n )
 *
 * DESCRIPTION  :  Print 'n' value
 *
 * PARAMETERS   :  int    n
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void tail_recursion_func( int n )
{
    if( n > 0 )
    {
        printf("%d ", n);
        tail_recursion_func( n - 1 );
    }
}


/* ********************************************************************************
 * NAME         :  void test_tail_recursion_func( int num )
 *
 * DESCRIPTION  :  Test tail recursion function
 *
 * PARAMETERS   :  int    num    ( An integer number )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_tail_recursion_func( int num )
{
    printf("Tail recursion result: \n");
    
    tail_recursion_func( num );
    
    printf("\n");
}
