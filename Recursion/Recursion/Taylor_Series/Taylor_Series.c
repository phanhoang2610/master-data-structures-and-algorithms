
//
//  Taylor_Series.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/13.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Taylor_Series.h"

/* ********************************************************************************
 * NAME         :  double e( int x, int n )
 *
 * DESCRIPTION  :  Calculates the e^x value with The Taylor Series
 *
 * PARAMETERS   :  int    x    ( Base number )
 *                 int    n    ( Number of examples )
 *
 * RETURN       :  e^x value
 *
 * NOTE         :  Taylor Series: e^x = 1 + x + x^2/2! + x^3/3! + ...
 *                 For more information:
 *                 https://www.mathsisfun.com/algebra/taylor-series.html
 *
 * ********************************************************************************/
double e( int x, int n )
{
    static double power = 1.0;
    static double factorial = 1.0;
    double result = 0.0;
    
    if ( n == 0 )
    {
        return 1;
    }
    
    result    = e( x, n - 1 );    // Recursion
    power     = power * x;
    factorial = factorial * n;
    
    return result + power / factorial;
}


/* ********************************************************************************
 * NAME         :  double e_hnr( int x, int n )
 *
 * DESCRIPTION  :  Calculates the e^x value with The Taylor Series using Horner's Rule
 *
 * PARAMETERS   :  int    x    ( Base number )
 *                 int    n    ( Number of examples )
 *
 * RETURN       :  e^x value
 *
 * NOTE         :  Read Horner's Rule for more information.
 *
 * ********************************************************************************/
double e_hnr( int x, int n )
{
    static double s = 1;

    if ( n == 0 )
    {
        return s;
    }

    s = 1 + ( x * s ) / n;

    return e_hnr( x, n - 1 );
}


/* ********************************************************************************
 * NAME         :  void test_taylor_series_cal_func( int x, int n )
 *
 * DESCRIPTION  :  Test function
 *
 * PARAMETERS   :  int    x    ( Base number )
 *                 int    n    ( Number of examples )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_taylor_series_cal_func( int x, int n )
{
    printf("Taylor Series Calculation Using Recursion Result:\n");
    printf("e^%d = %lf\n", x, e( x, n ));
    
    printf("Taylor Series Calculation Using Horner's Rule Result:\n");
    printf("e^%d = %lf\n", x, e_hnr( x, n ));
    
    printf("\n");
}
