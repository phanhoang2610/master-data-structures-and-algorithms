//
//  Taylor_Series.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/13.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Taylor_Series_h
#define Taylor_Series_h

#include <stdio.h>

double e( int, int );
double e_hnr( int, int ); // The Taylor Series using Horner's Rule
void test_taylor_series_cal_func( int , int );

#endif /* Taylor_Series_h */
