//
//  main.c
//  LearningAlgorithms_Recursion
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Learning. All rights reserved.
//

#include <stdio.h>

#include "main.h"

int main( int argc, const char * argv[] )
{
    test_tail_recursion_func( 5 );
    test_head_recursion_func( 5 );
    test_tree_recursion_func( 5 );
    test_indirect_recursion_func( 20 );
    test_nested_recursion_func( 95 );
    test_factorial_using_recursion_func( 5 );    // 5! = 120
    test_power_using_recursion_func( 2, 9 );     // 2^9 = 512
    test_taylor_series_cal_func( 2, 15 );        // Increases the. 'n' for better approximations. Ex: x = 10->15
    test_fibonacci_series_cal_func( 10 );
    test_combination_formular_func( 4, 2 );
    test_tower_of_hanoi_func( 4 );               // Tests with 3 disks
    
    return 0;
}
