//
//  Fibonacci_Series.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/13.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Fibonacci_Series_h
#define Fibonacci_Series_h

#include <stdio.h>

#define MAX_NUM 100

int fib( int );
void test_fibonacci_series_cal_func( int );

#endif /* Fibonacci_Series_h */
