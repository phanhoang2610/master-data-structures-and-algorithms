//
//  Fibonacci_Series.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/13.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Fibonacci_Series.h"

static int F_List[ MAX_NUM ];    // Fibonacci List

/* ********************************************************************************
 * NAME         :  int fib( int n )
 *
 * DESCRIPTION  :  Gets number in Fibonacci series
 *
 * PARAMETERS   :  int    n    ( Number of examples )
 *
 * RETURN       :  A number in Fibonacci series
 *
 * ********************************************************************************/
int fib( int n )
{
    if ( n <= 1 )
    {
         return n; // fib(0) = 0 and fib(1) = 1
    }
    return fib( n - 2 ) + fib( n - 1 );
}

/* ********************************************************************************
 * NAME         :  fib_list( int n )
 *
 * DESCRIPTION  :  Creates list of n Fibonacci numbers
 *
 * PARAMETERS   :  int    n    ( Number of examples )
 *
 * RETURN       :  A number in Fibonacci series
 *
 * ********************************************************************************/
int fib_list( int n )
{
    if ( n <= 1 )
    {
        F_List[ n ] = n;
        return n; // fib(0) = 0 and fib(1) = 1
    }
    else
    {
        if ( F_List[ n - 2 ] == -1 )
        {
            F_List[ n - 2 ] = fib_list( n - 2 );
        }
        
        if ( F_List[ n - 1 ] == -1 )
        {
            F_List[ n - 1 ] = fib_list( n - 1 );
        }
    
        F_List[ n ] = F_List[ n - 2 ] + F_List[ n - 1 ];
        
        return F_List[ n - 2 ] + F_List[ n - 1 ];
    }
}


/* ********************************************************************************
 * NAME         :  void test_fibonacci_series_cal_func( int num )
 *
 * DESCRIPTION  :  Test function
 *
 * PARAMETERS   :  int    num    ( Number of examples )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_fibonacci_series_cal_func( int num )
{
    // Initialize the F_List
    int cnt;
    for ( cnt = 0; cnt < MAX_NUM; cnt++ )
    {
        F_List[ cnt ] = -1;
    }
    
    printf("Fibonacci Series:\n");
    
    printf("%d\n", fib_list( num ));
    
    for (cnt = 0; cnt <= num; cnt++)
    {
        printf("%d ", F_List[ cnt ]);
    }
        
    printf("\n");
}
