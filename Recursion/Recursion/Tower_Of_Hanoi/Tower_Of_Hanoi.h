//
//  Tower_Of_Hanoi.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/15.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Tower_Of_Hanoi_h
#define Tower_Of_Hanoi_h

#include <stdio.h>

#define TOWER_A  'A'    // Tower A character
#define TOWER_B  'B'    // Tower B character
#define TOWER_C  'C'    // Tower C character

void ToH( int, char, char, char );
void test_tower_of_hanoi_func( int );


#endif /* Tower_Of_Hanoi_h */
