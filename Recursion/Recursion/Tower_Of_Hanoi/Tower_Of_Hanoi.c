//
//  Tower_Of_Hanoi.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/15.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Tower_Of_Hanoi.h"

/* ********************************************************************************
 *  NAME         :  void ToH( int n, char A, char B, char C )
 *
 *  DESCRIPTION  :  Solves the Tower of Hanoi.
 *
 *  PARAMETERS   :  int    n    ( n of nCr formula )
 *                 int    r    ( r of nCr formula )
 *
 *  RETURN       :  None
 *
 * ********************************************************************************/
void ToH( int n, char A, char B, char C )
{
    if ( n > 0 )
    {
        ToH( n - 1, A, C, B );
        printf("Move disk from Tower [ %c ] -> [ %c ]\n", A, C);
        ToH( n - 1, B, A, C );
    }
}


/* ********************************************************************************
 *  NAME         :  void test_tower_of_hanoi_func( int n )
 *
 *  DESCRIPTION  :  Test function
 *
 *  PARAMETERS   :  int    n    ( Number of disks )
 *
 *  RETURN       :  None
 *
 *  NOTE         :  Tower of Hanoi : Move all disks from Tower (A) to Tower (C)
 *
 *                             (A)            (B)           (C)
 *                              |              |             |
 *                              |              |             |
 *                              |              |             |
 *          Disk 1   ->       [###]            |             |
 *          Disk 2   ->     [#######]          |             |
 *          Disk 3   ->   [###########]________|_____________|_______
 *
 * ********************************************************************************/
void test_tower_of_hanoi_func( int n )
{
    ToH( n, TOWER_A, TOWER_B, TOWER_C );
}
