//
//  Tree_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Tree_Recursion_h
#define Tree_Recursion_h

#include <stdio.h>

void tree_recursion_func( int );
void test_tree_recursion_func( int );

#endif /* Tree_Recursion_h */
