//
//  Tree_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Tree_Recursion.h"

/* ********************************************************************************
 * NAME         :  void tree_recursion_func( int n )
 *
 * DESCRIPTION  :  Print 'n' value 
 *
 * PARAMETERS   :  int    n
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void tree_recursion_func( int n )
{
    if( n > 0 )
    {
        printf("%d ", n);
        tree_recursion_func( n - 1 );
        tree_recursion_func( n - 1 );
    }
}


/* ********************************************************************************
 * NAME         :  void test_tree_recursion_func( int num )
 *
 * DESCRIPTION  :  Tests tree recursion function
 *
 * PARAMETERS   :  int    num    ( An integer number )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_tree_recursion_func( int num )
{
    printf("Tree recursion result: \n");
    
    tree_recursion_func( num );
    
    printf("\n");
}
