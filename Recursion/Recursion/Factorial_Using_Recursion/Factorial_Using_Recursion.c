//
//  Factorial_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Factorial_Using_Recursion.h"

/* ********************************************************************************
 *  NAME         :  int factorial_using_recursion_func( int n )
 *
 *  DESCRIPTION  :  Calculates factotial value of an integer number
 *
 *  PARAMETERS   :  int    n
 *
 *  RETURN       :  Factorial value of an integer
 *
 * ********************************************************************************/
int factorial_using_recursion_func( int n )
{
    if ( n == 1 )
    {
        return 1;
    }
    else
    {
        return n * factorial_using_recursion_func( n - 1 );
    }
}

/* ********************************************************************************
 *  NAME         :  void test_factorial_using_recursion_func( int num )
 *
 *  DESCRIPTION  :  Test factotial using recursion function
 *
 *  PARAMETERS   :  int    num    ( An integer number )
 *
 *  RETURN       :  None
 *
 * ********************************************************************************/
void test_factorial_using_recursion_func( int num )
{
    int value = 0;
    
    printf("Factorial Using Recursion Result:\n");
    
    value = factorial_using_recursion_func( num );
    
    printf("%d! = %d", num, value);
    
    printf("\n");
}
