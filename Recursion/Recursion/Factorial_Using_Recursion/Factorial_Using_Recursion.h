//
//  Factorial_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Factorial_Using_Recursion_h
#define Factorial_Using_Recursion_h

#include <stdio.h>

int factorial_using_recursion_func( int );
void test_factorial_using_recursion_func( int );

#endif /* Factorial_Using_Recursion_h */
