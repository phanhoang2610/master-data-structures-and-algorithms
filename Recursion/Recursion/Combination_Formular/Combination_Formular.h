//
//  Combination_Formular.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/14.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Combination_Formular_h
#define Combination_Formular_h

#include <stdio.h>

int combination( int, int );
int combination_re1( int, int );
int combination_re2( int, int );

int factorial_cal( int );

void test_combination_formular_func( int, int );

#endif /* Combination_Formular_h */
