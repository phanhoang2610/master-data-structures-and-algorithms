//
//  Combination_Formular.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/14.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Combination_Formular.h"

/* ********************************************************************************
* NAME         :  int combination( int n, int r )
*
* DESCRIPTION  :  Calculates nCr value using iterative loops
*
* PARAMETERS   :  int    n    ( n of nCr formula )
*                 int    r    ( r of nCr formula )
*
* RETURN       :  nCr value
*
* ********************************************************************************/
int combination( int n, int r )
{
    int result;
    
    int fact_n = factorial_cal( n );    // Factorial of n (n!)
    int fact_r = factorial_cal( r );    // Factorial of r (r!)
    int fact_nr = factorial_cal( n - r ); // Calculates (n-r)!
    
    result = fact_n / (fact_r * fact_nr);
    
    return result;
}


/* ********************************************************************************
* NAME         :  int factorial_cal( int n )
*
* DESCRIPTION  :  Calculates factorial of an integer number
*
* PARAMETERS   :  int    n    ( n of nCr formula )
*                 int    r    ( r of nCr formula )
*
* RETURN       :  Factorial value of an integer number
*
* ********************************************************************************/
int factorial_cal( int n )
{
    int result = 1.0;
    
    int cnt;
    
    for ( cnt = 0 ; cnt < n ; cnt++ )
    {
        result = result * ( cnt + 1 );
    }
    
    return result;
}


/* ********************************************************************************
 * NAME         :  int combination_re1( int n, int r )
 *
 * DESCRIPTION  :  Calculates nCr value using recursion
 *
 * PARAMETERS   :  int    n    ( n of nCr formula )
 *                 int    r    ( r of nCr formula )
 *
 * RETURN       :  nCr value
 *
 * ********************************************************************************/
int combination_re1( int n, int r )
{
    static int fact_n = 1.0;         // n! value
    static int fact_r = 1.0;         // r! value
    static int fact_sub_nr = 1.0;    // (n - r)! value
    
    static int sub_nr;               // (n - r) value
    
    sub_nr = n - r; // Gets (n - r) value
        
    if ( n == 0 )
    {
        return 1;
    }
    
    // Calculates n!
    fact_n = fact_n * n;
    
    // Calculates r!
    if ( r > 0 )
    {
        fact_r = fact_r * r;
    }
    
    // Calculates (n - r)!
    if ( sub_nr >= n )
    {
        fact_sub_nr = fact_sub_nr * n;
    }
    
    // Recursion
    combination_re1( n - 1, r - 1 );
       
    return fact_n / ( fact_r * fact_sub_nr );
}


/* ********************************************************************************
 * NAME         :  int combination_re2( int n, int r )
 *
 * DESCRIPTION  :  Calculates nCr value using recursion and Pascal Triangle
 *
 * PARAMETERS   :  int    n    ( n of nCr formula )
 *                 int    r    ( r of nCr formula )
 *
 * RETURN       :  nCr value
 *
 * NOTE         :  Pascal Triangle ( nCr : when n = r or r = 0, return 1 )
 *
 *                 n=0          1                       0C0
 *                 n=1         1 1                   1C0   1C1
 *                 n=2        1 2 1               2C0   2C1   2C2
 *                 n=3       1 3 3 1           3C0   3C1   3C2   3C3
 *                 n=4      1 4 6 4 1       4C0   4C1   4C2   4C3   4C4
 *
 * ********************************************************************************/
int combination_re2( int n, int r )
{
    int a;
    if ( r == 0 || n == r )
    {
        a = 0;
        return 1;
    }
    else
    {
        a = 1;
        return combination_re2( n - 1, r - 1 ) + combination_re2( n - 1, r );
    }
}


/* ********************************************************************************
 * NAME         :  void test_combination_formular_func( int n, int r )
 *
 * DESCRIPTION  :  Test function
 *
 * PARAMETERS   :  int    n    ( n of nCr formula )
 *                 int    r    ( r of nCr formula )
 *
 * RETURN       :  None
 *
 * NOTE         :  Combination Formular : nCr=n!/(r!*(n-r)!)
 * ********************************************************************************/
void test_combination_formular_func( int n, int r )
{
    printf("Method 1: Combination nCr using iterative loops: %d\n", combination( n, r ));
    printf("Method 2: Combination nCr using recursion: %d\n", combination_re1( n, r ));
    printf("Method 3: Combination nCr using recursion and Pascal Triangle formula: %d\n", combination_re2( n, r ));
    
    printf("\n");
}
