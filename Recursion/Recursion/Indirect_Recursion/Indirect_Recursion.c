//
//  Indirect_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Indirect_Recursion.h"

/* ********************************************************************************
 * NAME         :  void func_A( int n )
 *
 * DESCRIPTION  :  When 'n' > 0, print 'n' value and call func_B
 *
 * PARAMETERS   :  int    n
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void func_A( int n )
{
    if ( n > 0 )
    {
        printf("%d ", n);
        func_B( n - 1 );
    }
}


/* ********************************************************************************
 * NAME         :  void func_B( int n )
 *
 * DESCRIPTION  :  When 'n' > 0, print 'n' value and call func_A
 *
 * PARAMETERS   :  int    n
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void func_B( int n )
{
    if ( n > 1 )
    {
        printf("%d ", n);
        func_A( n / 2 );
    }
}


/* ********************************************************************************
 * NAME         :  void test_indirect_recursion_func( int num )
 *
 * DESCRIPTION  :  Test indirect recursion function
 *
 * PARAMETERS   :  int    num    ( An integer number )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_indirect_recursion_func( int num )
{
    printf("Indirect recursion result:\n");
    
    func_A( num );
    
    printf("\n");
}
