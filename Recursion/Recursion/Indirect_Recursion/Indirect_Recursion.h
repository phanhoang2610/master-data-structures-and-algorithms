//
//  Indirect_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Indirect_Recursion_h
#define Indirect_Recursion_h

#include <stdio.h>

void func_A( int );
void func_B( int );
void test_indirect_recursion_func( int );

#endif /* Indirect_Recursion_h */
