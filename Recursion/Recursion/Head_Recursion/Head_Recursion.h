//
//  Head_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/13.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Head_Recursion_h
#define Head_Recursion_h

#include <stdio.h>

void head_recursion_func( int );
void test_head_recursion_func( int );

#endif /* Head_Recursion_h */
