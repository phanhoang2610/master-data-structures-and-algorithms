//
//  Tail_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Head_Recursion.h"

/* ********************************************************************************
 * NAME         :  void head_recursion_func( int n )
 *
 * DESCRIPTION  :  Print 'n' value
 *
 * PARAMETERS   :  int    n
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void head_recursion_func( int n )
{
    if( n > 0 )
    {
        head_recursion_func( n - 1 );
        printf("%d ", n);
    }
}


/* ********************************************************************************
 * NAME         :  void test_head_recursion_func( int num )
 *
 * DESCRIPTION  :  Test head recursion function
 *
 * PARAMETERS   :  int    num    ( An integer number )
 *
 * RETURN       :  None
 *
 * ********************************************************************************/
void test_head_recursion_func( int num )
{
    printf("Head recursion result: \n");
    
    head_recursion_func( num );
    
    printf("\n");
}
