//
//  Nested_Recursion.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef Nested_Recursion_h
#define Nested_Recursion_h

#include <stdio.h>

int nested_recursion_func( int );
void test_nested_recursion_func( int );

#endif /* Nested_Recursion_h */
