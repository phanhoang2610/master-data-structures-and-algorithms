//
//  Nested_Recursion.c
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#include "Nested_Recursion.h"

/* ********************************************************************************
 *  NAME         :  int nested_recursion_func( int n )
 *
 *  DESCRIPTION  :  Passes the parameter as a recursive call.
 *
 *  PARAMETERS   :  int    n
 *
 *  RETURN       :  Integer value
 *
 * ********************************************************************************/
int nested_recursion_func( int n )
{
    if ( n > 100 )
    {
        return ( n - 10 );
    }
    else
    {
        return nested_recursion_func( nested_recursion_func( n + 11 ) );
    }
}


/* ********************************************************************************
 *  NAME         :  void test_nested_recursion_func( int num )
 *
 *  DESCRIPTION  :  Test nested recursion function
 *
 *  PARAMETERS   :  int    num    ( An integer number )
 *
 *  RETURN       :  None
 *
 * ********************************************************************************/
void test_nested_recursion_func( int num )
{
    int value = 0;
    
    printf("Indirect recursion result:\n");

    value = nested_recursion_func( num );
    
    printf("%d ", value);
    
    printf("\n");
}
