//
//  main.h
//  LearningAlgorithms
//
//  Created by Phan Hoang on 2020/08/12.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

#ifndef main_h
#define main_h

#include <stdio.h>
#include <stdlib.h>

#include "Tail_Recursion.h"
#include "Head_Recursion.h"
#include "Tree_Recursion.h"
#include "Indirect_Recursion.h"
#include "Nested_Recursion.h"
#include "Factorial_Using_Recursion.h"
#include "Power_Using_Recursion.h"
#include "Taylor_Series.h"
#include "Fibonacci_Series.h"
#include "Combination_Formular.h"
#include "Tower_Of_Hanoi.h"

#endif /* main_h */
