# Master Data Structures And Algorithms with C/C++

Implementation of various popular Data Structures and their Algorithms using C and C++.

- IDE : Xcode 11.6
- Programing Language : C/C++
- OS: macOS Catalina 10.15.6

## Recursion
1. Tail Recursion               【 末尾再帰(まつびさいき) 】
2. Head Recursion               【 頭の再帰(あたまのさいき) 】
3. Tree Recursion               【 ツリーの再帰(ツリーのさいき) 】
4. Indirect Recursion           【 間接再帰(かんせつさいき) 】
5. Nested Recursion             【 ネスト再帰(ネストさいき) 】
6. Factorial Using Recursion    【 再帰関数で階乗(かいじょう)計算 】
7. Power Using Recursion        【 再帰関数で累乗(るいじょう)計算 】
8. Taylor Series                【 再帰関数でテイラー級数(きゅうすう)計算 】
9. Fibonacci Series             【 再帰関数でフィボナッチ数列作成 】
10. Combination Series          【 再帰関数で組み合わせの数(nCr)計算 】
11. Tower of Hanoi              【 ハノイの塔(とう)を再帰で解く 】

## Array ADT ( Abstract Data Type ) (抽象データ型(ちゅうしょうデータがた))
1. Display                   【 表示 】
2. Add/Append             　 【 追加 】
3. Insert                    【 挿入 】
4. Delete　                  【 削除 】
5. Search (Linear/Binary)    【 検索 】
6. Get                     　【 取得 】
7. Set　                     【 設定 】
8. Max/Min                   【 最大値・最小値 】
9. Reverse                   【 逆行列 】
10. Get duplicates           【 重複の検索 】

